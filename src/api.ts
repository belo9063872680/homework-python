import axios from "axios";

const API_ENDPOINT = "https://bel1999.pythonanywhere.com";

interface AuthResponse {
  auth_token: string;
}

export interface Book {
  id: number;
  title: string;
  publication_year: number;
  author: number;
  genre: number;
}

export const $axios = axios.create({
  withCredentials: true,
  baseURL: API_ENDPOINT,
});

$axios.interceptors.request.use((config) => {
  if (!config.headers) {
    return;
  }

  config.headers.Authorization = `Token ${localStorage.getItem("token")}`;
  return config;
});

export const api = {
  async login(username: string, password: string): Promise<string> {
    const loginFormData = new FormData();
    loginFormData.append("username", username);
    loginFormData.append("password", password);

    const response = await axios({
      method: "post",
      url: `${API_ENDPOINT}/auth/token/login`,
      data: loginFormData,
      headers: {
        "Content-Type": `multipart/form-data`,
      },
    });

    const { auth_token }: AuthResponse = await response.data;
    return auth_token;
  },

  async logout() {
    return await $axios.post(`/auth/token/logout/`);
  },

  async getBooks(): Promise<Book[]> {
    const response = await $axios.get("/api/v1/book/");
    return response.data;
  },
};
