import React, {
  createContext,
  ReactNode,
  useContext,
  useMemo,
  useState,
} from "react";
import { api } from "../api";
import { useEffect } from "react";

interface IAuthContext {
  loading: boolean;
  isAuth: boolean;
  error?: any;
  login: (email: string, password: string) => void;
  logout: () => void;
}

const AuthContext = createContext<IAuthContext>({} as IAuthContext);

// Export the provider as we need to wrap the entire app with it
export function AuthProvider({
  children,
}: {
  children: ReactNode;
}): JSX.Element {
  const [isAuth, setIsAuth] = useState(false);
  const [error, setError] = useState<string | null>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    if (localStorage.getItem("token")) {
      setIsAuth(true);
    }
  }, []);

  const login = async (username: string, password: string) => {
    try {
      setLoading(true);
      const token = await api.login(username, password);
      localStorage.setItem("token", token);
      setIsAuth(true);
    } catch (e) {
      setError(`Authentication error:${e}`);
    } finally {
      setLoading(false);
    }
  };

  const logout = async () => {
    try {
      setLoading(true);
      const response = await api.logout();

      if (response.status !== 204) {
        throw new Error(
          `connection error with status code:${response.statusText}`
        );
      }

      localStorage.removeItem("token");
      setIsAuth(false);
    } catch (e) {
      setError(`Authentication error:${e}`);
    } finally {
      setLoading(false);
    }
  };

  const memoedValue = useMemo(
    () => ({
      isAuth,
      loading,
      error,
      login,
      logout,
    }),
    [isAuth, loading, error]
  );

  return (
    <AuthContext.Provider value={memoedValue}>{children}</AuthContext.Provider>
  );
}

export function useAuth() {
  return useContext(AuthContext);
}
