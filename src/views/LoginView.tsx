import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "./LoginView.css";
import { useCallback, useState } from "react";
import { useAuth } from "../hooks/Auth";
import { Alert, Spinner } from "react-bootstrap";

const DEFAULT_USERNAME = "admin";
const DEFAULT_PASSWORD = "admin";

export const LoginView = () => {
  const [username, setUsername] = useState<string>(DEFAULT_USERNAME);
  const [password, setPassword] = useState<string>(DEFAULT_PASSWORD);

  const { loading, login, error } = useAuth();

  const onSubmit = useCallback(
    async (e: React.FormEvent) => {
      e.preventDefault();

      login(username, password);
    },
    [login, password, username]
  );

  const onChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value);
  };

  const onChangeUsername = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(e.target.value);
  };

  if (loading) {
    return (
      <div className="login-container">
        <Spinner animation="border" variant="primary" />
      </div>
    );
  }

  return (
    <div className="login-container">
      <Form onSubmit={onSubmit}>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Логин</Form.Label>
          <Form.Control
            type="login"
            placeholder="Введите логин"
            value={username}
            onChange={onChangeUsername}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Пароль</Form.Label>
          <Form.Control
            type="password"
            placeholder="Введите пароль"
            value={password}
            onChange={onChangePassword}
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Войти
        </Button>

        {error && (
          <Alert className="form__error" variant="danger">
            {error}
          </Alert>
        )}
      </Form>
    </div>
  );
};
