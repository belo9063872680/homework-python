import React, { useMemo, useState, useCallback, useEffect } from "react";
import "./MainView.css";
import { Header } from "../components/Header";
import { Container, Spinner } from "react-bootstrap";
import { BookCard } from "../components/BookCard";
import { Pagination } from "../components/Pagination";
import { api, Book } from "../api";

const BOOKS_PER_PAGE = 4;

export function MainView() {
  const [books, setBooks] = useState<Book[]>([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    const getBooks = async () => {
      setLoading(true);
      const books = await api.getBooks();
      setBooks(books);
      setLoading(false);
    };

    getBooks();
  }, []);

  const indexOfLastBook = useMemo(
    () => currentPage * BOOKS_PER_PAGE,
    [currentPage]
  );
  const indexOfFirstBook = useMemo(
    () => indexOfLastBook - BOOKS_PER_PAGE,
    [indexOfLastBook]
  );
  const currentBooks = useMemo(
    () => books.slice(indexOfFirstBook, indexOfLastBook),
    [books, indexOfFirstBook, indexOfLastBook]
  );

  const paginate = useCallback(
    (pageNumber: number) => setCurrentPage(pageNumber),
    []
  );

  return (
    <>
      {loading ? (
        <div className="main-view">
          <Header />
          <main className="main-view__container">
            <Container className="main-view__content main-view__content--spinner">
              <Spinner animation="border" variant="primary" />
            </Container>
          </main>
        </div>
      ) : (
        <div className="main-view">
          <Header />
          <main className="main-view__container">
            <Container className="main-view__content">
              <div className="main-view__grid">
                {currentBooks.map((book) => (
                  <BookCard key={book.id} {...book} />
                ))}
              </div>
              <Pagination
                className="main-view__pagination"
                itemsPerPage={BOOKS_PER_PAGE}
                totalItems={books.length}
                paginate={paginate}
              ></Pagination>
            </Container>
          </main>
        </div>
      )}
    </>
  );
}
