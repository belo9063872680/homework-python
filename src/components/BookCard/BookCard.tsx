import { memo } from "react";
import Card from "react-bootstrap/Card";
import { Book } from "../../api";
import "./BookCard.css";

const IMG_SRC = "https://picsum.photos/200/300";

export const BookCard = memo(
  ({ title, publication_year }: Book): JSX.Element => {
    return (
      <Card className="book-card" style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          className="book-card__img"
          src={IMG_SRC}
          alt={`${title} img`}
        />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
          <Card.Text>{publication_year}</Card.Text>
        </Card.Body>
      </Card>
    );
  }
);
