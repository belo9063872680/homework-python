import { Button, Container, Navbar, Spinner } from "react-bootstrap";
import { useAuth } from "../../hooks/Auth";
import "./Header.css";

export const Header = () => {
  const { logout, loading } = useAuth();

  const onClick = () => {
    logout();
  };

  return (
    <Navbar bg="light" expand="lg" className="navbar">
      <Container>
        <Navbar.Brand>Библиотека</Navbar.Brand>
        <div className="navbar__logout logout-navbar">
          {loading && (
            <Spinner
              animation="border"
              variant="primary"
              size="sm"
              className="logout-navbar__spinner"
            />
          )}
          <Button onClick={onClick}>Выйти</Button>
        </div>
      </Container>
    </Navbar>
  );
};
