import React from "react";
import { LoginView } from "./views/LoginView";
import "./App.css";
import { MainView } from "./views/MainView";
import { useAuth } from "./hooks/Auth";
function App() {
  const { isAuth } = useAuth();

  return <div className="App">{isAuth ? <MainView /> : <LoginView />}</div>;
}

export default App;
